FROM php:8.1.5-alpine
EXPOSE 8080

RUN	echo "upload_max_filesize = 128M" >> /usr/local/etc/php/conf.d/0-upload_large_dumps.ini \
&&	echo "post_max_size = 128M" >> /usr/local/etc/php/conf.d/0-upload_large_dumps.ini \
&&	echo "memory_limit = 1G" >> /usr/local/etc/php/conf.d/0-upload_large_dumps.ini \
&&	echo "max_execution_time = 600" >> /usr/local/etc/php/conf.d/0-upload_large_dumps.ini \
&&	echo "max_input_vars = 5000" >> /usr/local/etc/php/conf.d/0-upload_large_dumps.ini

STOPSIGNAL SIGINT

RUN	addgroup -g 1000 app \
&& adduser -u 1000 -G app -D app \
&& mkdir -p /app \
&& mkdir -p /app/plugins-enabled \
&& chown -R 1000:1000 /app

WORKDIR /app

RUN	set -x \
&&	apk add --no-cache --virtual .build-deps \
	postgresql-dev \
	sqlite-dev \
&&	docker-php-ext-install \
	pdo_mysql \
	pdo_pgsql \
	pdo_sqlite \
&&	runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)" \
&&	apk add --virtual .phpexts-rundeps $runDeps \
&&	apk del .build-deps

ENV	ADMINER_VERSION 4.8.1
ENV	ADMINER_DOWNLOAD_SHA256 2fd7e6d8f987b243ab1839249551f62adce19704c47d3d0c8dd9e57ea5b9c6b3
ENV	ADMINER_SRC_DOWNLOAD_SHA256 ef832414296d11eed33e9d85fff3fb316c63f13f05fceb4a961cbe4cb2ae8712

RUN	set -x \
 && curl -fsSL https://github.com/vrana/adminer/releases/download/v$ADMINER_VERSION/adminer-$ADMINER_VERSION.php -o adminer.php \
 && echo "$ADMINER_DOWNLOAD_SHA256  adminer.php" |sha256sum -c - \
 && mv adminer.php index.php

USER app
CMD [ "php", "-S", "[::]:8080", "-t", "/app" ]
