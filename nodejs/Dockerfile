FROM node:16.15.0-bullseye-slim as runner_base

RUN userdel -f node \
 && groupadd --gid 1000 app \
 && useradd --uid 1000 --gid app --create-home app \
 && mkdir -p /app \
 && chown -R 1000:1000 /app

USER app


FROM runner_base as chrome

USER root

RUN usermod -a -G audio,video app

# https://omahaproxy.appspot.com/
ENV CHROME_VERSION=101.0.4951.64-1

RUN apt-get update -qqy \
  && apt-get -qqy install \
       unzip gnupg wget ca-certificates apt-transport-https \
       ttf-wqy-zenhei \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*
# dependencies https://github.com/puppeteer/puppeteer/blob/main/.ci/node12/Dockerfile.linux
RUN apt-get update && \
    apt-get -y install xvfb gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 \
      libdbus-1-3 libexpat1 libfontconfig1 libgbm1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
      libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
      libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 \
      libxtst6 ca-certificates fonts-liberation libnss3 lsb-release xdg-utils wget && \
    rm -rf /var/lib/apt/lists/*
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-cache policy google-chrome-stable \
  && apt-get -qqy install \
    google-chrome-stable=${CHROME_VERSION} \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

ENV CHROME_BIN=/usr/bin/google-chrome

USER app
