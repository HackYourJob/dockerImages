# https://hub.docker.com/_/microsoft-dotnet-aspnet/
FROM mcr.microsoft.com/dotnet/aspnet:6.0.5-bullseye-slim-amd64
WORKDIR /app
EXPOSE 8080
ENV SERVER_HOST "http://0.0.0.0:8080"

RUN groupadd --gid 1000 app \
 && useradd --uid 1000 --gid app --create-home app \
 && mkdir -p /app \
 && chown -R 1000:1000 /app

USER app
