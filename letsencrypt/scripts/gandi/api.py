#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pprint
import requests
import os
import json
import tldextract

class TxtRecordApi(object):
    apiUri = ""
    headers = {}
    ttl = 300

    def __init__(self, apikey, domain, recordName):
        self.apiUri = "https://api.gandi.net/v5/livedns/domains/" + domain + "/records/" + recordName + "/TXT"

        self.headers = {
            "Authorization": "ApiKey " + apikey,
        }

    def getValues(self):
        response = requests.get(self.apiUri, headers=self.headers)
        if response.status_code == 404:
            return []
        elif response.status_code == 200:
            record = response.json()
            return record["rrset_values"]
        else:
            print("Failed to look for existing " + txt_name + " record")
            response.raise_for_status()
            exit(1)

    def printIfError(self, response):
        if not response.ok:
            print("something went wrong")

            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(response.content)
            response.raise_for_status()

    def create(self, value):
        newrecord = {
            "rrset_ttl": self.ttl,
            "rrset_values": [value]
        }

        response = requests.post(self.apiUri, headers=self.headers, json=newrecord)
        self.printIfError(response)
        return response.ok

    def update(self, values):
        newrecord = {
            "rrset_ttl": self.ttl,
            "rrset_values": values
        }

        response = requests.put(self.apiUri, headers=self.headers, json=newrecord)
        self.printIfError(response)
        return response.ok

    def append(self, newValue):
        oldValues = self.getValues()
        if len(oldValues) > 0:
            print("Update record")
            return self.update(oldValues + [newValue])
        else:
            print("Create record")
            return self.create(newValue)

    def remove(self):
        response = requests.delete(self.apiUri, headers=self.headers)
        if response.status_code == 404:
            return True
        else:
            self.printIfError(response)
            return response.ok

class Config(object):
    certbot_domain = ""
    certbot_validation = ""
    apikey = ""

    def __init__(self, apikey):
        self.certbot_domain = os.environ.get("CERTBOT_DOMAIN")
        self.certbot_validation = os.environ.get("CERTBOT_VALIDATION")
        self.apikey = apikey

    def checkConfig(self):
        try:
            self.certbot_domain
        except NameError:
            print("CERTBOT_DOMAIN environment variable is missing, exiting")
            exit(1)

        try:
            self.certbot_validation
        except NameError:
            print("CERTBOT_VALIDATION environment variable is missing, exiting")
            exit(1)

    def buildApi(self):
        domainParts = tldextract.extract(self.certbot_domain.replace("*.", ""))
        domain = domainParts.domain + "." + domainParts.suffix
        subdomain = domainParts.subdomain
        recordName = ("_acme-challenge." + subdomain) if subdomain else "_acme-challenge"

        return TxtRecordApi(self.apikey, domain, recordName)

    def getToken(self):
        return self.certbot_validation
