#!/bin/sh

set -e

/usr/local/bin/certbot \
    certonly \
    --server https://acme-v02.api.letsencrypt.org/directory \
    --manual \
    --manual-public-ip-logging-ok \
    --preferred-challenges dns \
    -d $@

./exportCertificates.sh
