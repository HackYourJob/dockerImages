#!/bin/sh

set -e

for d in `ls /etc/letsencrypt/live/`;
do
    if [ $d != "README" ]
    then
        ( echo $d && cat /etc/letsencrypt/live/$d/privkey.pem /etc/letsencrypt/live/$d/fullchain.pem > /app/certificates/$d.pem  )
    fi
done
