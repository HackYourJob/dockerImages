FROM php:7.3.33-fpm-bullseye as base-73
ENV BUILD_DEPS \
        zlib1g-dev \
        git \
        libgmp-dev \
        unzip \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        build-essential \
        chrpath \
        libssl-dev \
        libxft-dev \
        libfreetype6 \
        libfontconfig1 \
        libfontconfig1-dev \
        ca-certificates \
        libzip-dev

RUN apt-get update \
 && apt-get install -y --no-install-recommends $BUILD_DEPS \
 && update-ca-certificates \
 && ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/ \
 && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
 && docker-php-ext-configure gmp \
 && docker-php-ext-install iconv mbstring pdo pdo_mysql zip gd gmp opcache \
 && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd --gid 1000 app \
 && useradd --uid 1000 --gid app --create-home app \
 && mkdir -p /app \
 && chown -R 1000:1000 /app


FROM base-73 as builder-73
RUN apt-get update \
 && apt-get install -y --no-install-recommends curl wget gnupg dirmngr xz-utils libatomic1 \
 && update-ca-certificates \
 && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN curl -sS https://raw.githubusercontent.com/composer/getcomposer.org/650bee119e1f3b87be1b787fe69a826f73dbdfb9/web/installer -o composer-setup.php \
 && php composer-setup.php --version="1.10.26" --install-dir=/usr/local/bin --filename=composer \
 && composer --version \
 && rm composer-setup.php

WORKDIR /app
USER app


FROM base-73 as runner-73
# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
} > /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

CMD ["php-fpm"]
WORKDIR /app
USER app
